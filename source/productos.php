<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Tienda | Lookuma</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/style-slider.css">
	<link rel="stylesheet" type="text/css" href="fuentes/icons.css">

</head>
<body>
	<header class="centrar">
		<article class="content-user relative">
			<div class="user">
				<span class="my-count saludo">Hola Ronny</span>

				<div class="my-count">
					<a href="" class="enlace count-user">Mi cuenta</a>
				</div>				
			</div>
			<figure class="lookuma-logo-min">
				 <img src="images/logo-lookuma.png" alt="lookuma" class="log-min">				 
			</figure>

			<div class="register">
				<div class="my-count saludo">
					<a href="" class="enlace count-user">Inicia sesión</a>
				</div>
				<div class="my-count">
					<a href="" class="enlace count-user">Regístrate</a>
				</div>
			</div>
		</article>
		
		<section class="head-center relative">
			<article class="logotype">
			<figure class="isotype">
				<img src="images/logo-lookuma.png" alt="lookuma | tienda online">
			</figure>
			</article>
			<article class="cart">
				<span class="carrito">Mi Carrito</span>
				<a href="" class="items">Items (120)</a>
			</article>
		</section>		
	</header>

	<nav class="centrar">
		<a href="" class="button-menu">Menú <i class="icon-menu2"></i></a>
		<ul class="menu-principal">
			<li class="list first">
				<a href="">Lentes</a>
				<ul class="sub-menu">
					<li class="sub-list">
						<a href="">Hombre</a>
					</li>
					<li class="sub-list">
						<a href="">Mujer</a>
					</li>					
				</ul>
			</li>
			<li class="list">
				<a href="">Carteras</a>
			</li>
			<li class="list">
				<a href="">Relojes</a>
			</li>
			<li class="list">
				<a href="">Perfumes</a>
			</li>
			<li class="list">
				<a href="">Otros</a>
				<ul class="sub-menu">
					<li class="sub-list">
						<a href="">Cartucheras</a>
					</li>
					<li class="sub-list">
						<a href="">Lapiceros</a>
					</li>
					<li class="sub-list">
						<a href="">accesorios</a>
					</li>
					<li class="sub-list">
						<a href="">accesorios</a>
					</li>				
				</ul>
			</li>
		</ul>	
	</nav>
	<section class="divitions-catego centrar">		
			<ul>
				<li class="list-gen link-first"><a href="#" class="active">Hombre</a></li>
				<li class="list-gen"><a href="#">Mujer</a></li>
			</ul>		
	</section>
	<section class="container centrar">
		<main class="main-list">
			<aside class="col-left">
				<h2 class="sub-cat-tittle">Lentes</h2>
				<ul class="left-menu">
					<li class="list-left"><a href="#" class="active">Ray-Ban</a></li>
					<li class="list-left"><a href="#">Oakley</a></li>
					<li class="list-left"><a href="#">DVF</a></li>
					<li class="list-left"><a href="#">Spy</a></li>
					<li class="list-left"><a href="#">Gucci</a></li>
					<li class="list-left"><a href="#">Prada</a></li>
				</ul>
			</aside>
			<section class="col-right">
				<article class="product relative">
					<div class="image-product">									
						<figure class="content-zoom">
							<a href="#"><img class="zoom" src="imagenes/productos/producto-1.jpg" alt=""></a>													
						</figure>					
						<img class="agotado"src="images/producto-agotado.png" alt="">
						<img class="oferta" src="images/producto-oferta.png" alt="">
					</div>				
						<h3 class="txt-product"><a href="">Lentes Ray Ban Aviador Lunas Rojas</a></h3>
						<a href="#" class="button-ver">
							Ver más
						</a>
						<div class="prod-price">
							<del>S/.390</del>
		                    <span>Precio:</span><ins>S/.1299</ins>
	                	</div>				
				</article>
				<article class="product relative">
					<div class="image-product">									
						<figure class="content-zoom">
							<a href="#"><img class="zoom" src="imagenes/productos/producto-1.jpg" alt=""></a>													
						</figure>					
						<img class="agotado"src="images/producto-agotado.png" alt="">						
					</div>				
						<h3 class="txt-product"><a href="">Lentes Ray Ban Aviador Lunas Rojas</a></h3>
						<a href="#" class="button-ver">
							Ver más
						</a>
						<div class="prod-price">
							<del>S/.390</del>
		                    <span>Precio:</span><ins>S/.1299</ins>
	                	</div>				
				</article>
				<article class="product relative">
					<div class="image-product">									
						<figure class="content-zoom">
							<a href="#"><img class="zoom" src="imagenes/productos/producto-1.jpg" alt=""></a>													
						</figure>
						<img class="oferta" src="images/producto-oferta.png" alt="">
					</div>				
						<h3 class="txt-product"><a href="">Lentes Ray Ban Aviador Lunas Rojas</a></h3>
						<a href="#" class="button-ver">
							Ver más
						</a>
						<div class="prod-price">
							<del>S/.390</del>
		                    <span>Precio:</span><ins>S/.1299</ins>
	                	</div>				
				</article>
				<div class="space"></div>
				<article class="product relative">
					<div class="image-product">									
						<figure class="content-zoom">
							<a href="#"><img class="zoom" src="imagenes/productos/producto-1.jpg" alt=""></a>													
						</figure>	
					</div>				
						<h3 class="txt-product"><a href="">Lentes Ray Ban Aviador Lunas Rojas</a></h3>
						<a href="#" class="button-ver">
							Ver más
						</a>
						<div class="prod-price">
							<del>S/.390</del>
		                    <span>Precio:</span><ins>S/.1299</ins>
	                	</div>				
				</article>
				<div class="space"></div>
			</main>
			
		</section>
	</section>
	<footer class="footer centrar">
		<section class="redes relative">
			<div class="linea"></div>
			<div class="social-net">
				<div class="icon-facebook-img"><a href="#"></a></div>
				<div class="icon-pinterest-img"><a href="#"></a></div>
				<div class="icon-instagram-img"><a href="#"></a></div>
			</div>			
		</section>
		<section>
			<article class="about-lookuma inline-block">
				<span class="foot-title">Acerca de Lookuma</span>				
				<p>Vendemos una variedad de productos de marcas reconicidas, nuevas y originales entre los que destacan: perfumes, cremas corporales, lentes, relojes y lencería. además, promovemos marcas locales de muy buena calidad.</p>
				
			</article>
			<article class="foot-cat inline-block">
				<span class="foot-title">Categorías</span>
				<ul class="foot-menu">
					<li class="foot-list"><a href="">Lentes</a></li>
					<li class="foot-list"><a href="">Carteras</a></li>
					<li class="foot-list"><a href="">Relojes</a></li>
					<li class="foot-list"><a href="">Perfumes</a></li>
				</ul>
			</article>
			
			<article class="foot-services inline-block">
				<span class="foot-title">Servicios y Soporte</span>
				<ul class="foot-menu">
					<li class="foot-list"><a href="">Mapa del Sitio</a></li>
					<li class="foot-list"><a href="">Contáctanos</a></li>
					<li class="foot-list"><a href="">Información de delivery</a></li>
					<li class="foot-list"><a href="">Políticas de delivery</a></li>
					<li class="foot-list"><a href="">Políticas de privacidad</a></li>
					<li class="foot-list"><a href="">Términos y condiciones</a></li>
					<li class="foot-list"><a href="">Mi cuenta</a></li>
				</ul>
			</article>
		</section>		
	</footer>
	<a class="to-top">Subir</a>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/application.js" type="text/javascript"></script>
</body>
</html>