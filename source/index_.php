<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<div id="slider">
    <ul id="image-content" class="image-content">
        <?php
            $images = array(
                'banner-1.jpg',
                'banner-1.jpg',
                'banner-1.jpg',
                'banner-1.jpg',
                'banner-1.jpg',
                'banner-1.jpg'
            );
            $i=1;
            foreach($images as $img){
                if($i==1){
                    echo '<li class="image-content-item current" data-slider="'.$i.'"><img src="imagenes/banners/'.$img.'" alt=""></li>';
                }else{
                    echo '<li class="image-content-item" data-slider="'.$i.'"><img src="imagenes/banners/'.$img.'" alt=""></li>';
                }
                $i++;
            }
        ?>
    </ul>
    <div id="controls">
        <ul id="control-content" class="control-content">
            <li class="control-content-item dots">
                <ul id="dot-content" class="dot-content"></ul>
            </li>
            <li class="control-content-item">
                <a href="#" class="left"></a>
            </li>
            <li class="control-content-item">
                <a href="#" class="pause">Pause</a>
                <a href="#" class="play" style="display: none;">Play</a>
            </li>
            <li class="control-content-item">
                <a href="#" class="right">Right</a>
            </li>
        </ul>
    </div>
</div>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/application.js" type="text/javascript"></script>
</body>
</html>