<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Tienda | Lookuma</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/style-slider.css">
	<link rel="stylesheet" type="text/css" href="fuentes/icons.css">

</head>
<body>
	<header class="centrar">
		<article class="content-user relative">
			<div class="user">
				<span class="my-count saludo">Hola Ronny</span>

				<div class="my-count">
					<a href="" class="enlace count-user">Mi cuenta</a>
				</div>				
			</div>
			<figure class="lookuma-logo-min">
				 <img src="images/logo-lookuma.png" alt="lookuma" class="log-min">				 
			</figure>

			<div class="register">
				<div class="my-count saludo">
					<a href="" class="enlace count-user">Inicia sesión</a>
				</div>
				<div class="my-count">
					<a href="" class="enlace count-user">Regístrate</a>
				</div>
			</div>
		</article>
		
		<section class="head-center relative">
			<article class="logotype">
			<figure class="isotype">
				<img src="images/logo-lookuma.png" alt="lookuma | tienda online">
			</figure>
			</article>
			<article class="cart">
				<span class="carrito">Mi Carrito</span>
				<a href="" class="items">Items (120)</a>
			</article>
		</section>		
	</header>

	<nav class="centrar">
		<a href="" class="button-menu">Menú <i class="icon-menu2"></i></a>
		<ul class="menu-principal">
			<li class="list first">
				<a href="">Lentes</a>
				<ul class="sub-menu">
					<li class="sub-list">
						<a href="">Hombre</a>
					</li>
					<li class="sub-list">
						<a href="">Mujer</a>
					</li>					
				</ul>
			</li>
			<li class="list">
				<a href="">Carteras</a>
			</li>
			<li class="list">
				<a href="">Relojes</a>
			</li>
			<li class="list">
				<a href="">Perfumes</a>
			</li>
			<li class="list">
				<a href="">Otros</a>
				<ul class="sub-menu">
					<li class="sub-list">
						<a href="">Cartucheras</a>
					</li>
					<li class="sub-list">
						<a href="">Lapiceros</a>
					</li>
					<li class="sub-list">
						<a href="">accesorios</a>
					</li>
					<li class="sub-list">
						<a href="">accesorios</a>
					</li>				
				</ul>
			</li>
		</ul>	
	</nav>
	<section class="banners-slides centrar relative">
		<div id="slider">
		    <ul id="image-content" class="image-content">
		        <?php
		            $images = array(
		                'banner-1.jpg',
		                'banner-1.jpg',
		                'banner-1.jpg',
		                'banner-1.jpg',
		                'banner-1.jpg',
		                'banner-1.jpg'
		            );
		            $i=1;
		            foreach($images as $img){
		                if($i==1){
		                    echo '<li class="image-content-item current" data-slider="'.$i.'"><img src="imagenes/banners/'.$img.'" alt=""></li>';
		                }else{
		                    echo '<li class="image-content-item" data-slider="'.$i.'"><img src="imagenes/banners/'.$img.'" alt=""></li>';
		                }
		                $i++;
		            }
		        ?>
		    </ul>
		    <div id="controls">
		        <ul id="control-content" class="control-content">
		            <li class="control-content-item dots">
		                <ul id="dot-content" class="dot-content"></ul>
		            </li>
		            <li class="control-content-item">
		                <a href="#" class="left"></a>
		            </li>
		            <li class="control-content-item">
		                <a href="#" class="pause"></a>
		                <a href="#" class="play" style="display: none;"></a>
		            </li>
		            <li class="control-content-item">
		                <a href="#" class="right"></a>
		            </li>
		        </ul>
		    </div>
		</div>
		
	</section>
	<section class="container centrar">
		<article class="categories relative">
			<a href="#">
				<h3 class="txt-category">Lentes</h3>
				<figure class="image">
					<img src="imagenes/categorias/lentes-lookuma.jpg" alt="">
				</figure>
			</a>
		</article>
		<article class="categories relative">
			<a href="#">
				<h3 class="txt-category">Carteras</h3>
				<figure class="image">
					<img src="imagenes/categorias/carteras-lookuma.jpg" alt="">
				</figure>
			</a>
		</article>
		<article class="categories relative">
			<a href="#">
				<h3 class="txt-category">Relojes</h3>
				<figure class="image">
					<img src="imagenes/categorias/relojes-lookuma.jpg" alt="">
				</figure>
			</a>
		</article>
		<article class="categories relative">
			<a href="#">
				<h3 class="txt-category">Perfumes</h3>
				<figure class="image">
					<img src="imagenes/categorias/perfumes-lookuma.jpg" alt="">
				</figure>
			</a>
		</article>
		
	</section>
	<footer class="footer centrar">
		<section class="redes relative">
			<div class="linea"></div>
			<div class="social-net">
				<div class="icon-facebook-img"><a href="#"></a></div>
				<div class="icon-pinterest-img"><a href="#"></a></div>
				<div class="icon-instagram-img"><a href="#"></a></div>
			</div>			
		</section>
		<section>
			<article class="about-lookuma inline-block">
				<span class="foot-title">Acerca de Lookuma</span>				
				<p>Vendemos una variedad de productos de marcas reconicidas, nuevas y originales entre los que destacan: perfumes, cremas corporales, lentes, relojes y lencería. además, promovemos marcas locales de muy buena calidad.</p>
				
			</article>
			<article class="foot-cat inline-block">
				<span class="foot-title">Categorías</span>
				<ul class="foot-menu">
					<li class="foot-list"><a href="">Lentes</a></li>
					<li class="foot-list"><a href="">Carteras</a></li>
					<li class="foot-list"><a href="">Relojes</a></li>
					<li class="foot-list"><a href="">Perfumes</a></li>
				</ul>
			</article>
			
			<article class="foot-services inline-block">
				<span class="foot-title">Servicios y Soporte</span>
				<ul class="foot-menu">
					<li class="foot-list"><a href="">Mapa del Sitio</a></li>
					<li class="foot-list"><a href="">Contáctanos</a></li>
					<li class="foot-list"><a href="">Información de delivery</a></li>
					<li class="foot-list"><a href="">Políticas de delivery</a></li>
					<li class="foot-list"><a href="">Políticas de privacidad</a></li>
					<li class="foot-list"><a href="">Términos y condiciones</a></li>
					<li class="foot-list"><a href="">Mi cuenta</a></li>
				</ul>
			</article>
		</section>		
	</footer>
	<a class="to-top">Subir</a>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/application.js" type="text/javascript"></script>
</body>
</html>