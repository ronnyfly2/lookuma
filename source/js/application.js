var slider = {
    init: function(){
        var tempo = 5000;
        slider.createDots();
        $('.left').on('click', function(e){
            e.preventDefault();
            slider.setAutoMove(false);
            slider.moveTo($('.current'), '<-');
        });
        $('.right').on('click', function(e){
            e.preventDefault();
            slider.setAutoMove(false);
            slider.moveTo($('.current'), '->');
        });
        $('.pause').on('click', function(e){
            e.preventDefault();
            slider.setAutoMove(false);
        });
        $('.play').on('click', function(e){
            e.preventDefault();
            slider.setAutoMove(true, tempo);
        });
        $('.dot').on('click', function(e){
            e.preventDefault();
            $('.dotActive').removeClass('dotActive')
            slider.setAutoMove(false);
            var slideNum = $(this).parent().data('slider');
            var currentSlider = $('.dot-content').find('li[data-slider="'+slideNum+'"]');
            slider.moveTo(currentSlider, '.');
            $(this).addClass('dotActive')
        });
        slider.setAutoMove(true, tempo);
    },
    createDots: function(){
        var limitSliderNum = $('.image-content-item').length;
        var items = '';
        for(var i=1;i<=limitSliderNum;i++){
            if(i==1){
                items = items + '<li class="dot-content-item" data-slider="'+i+'"><a href="#" class="dot dotActive">'+i+'</a></li>'
            }else{
                items = items + '<li class="dot-content-item" data-slider="'+i+'"><a href="#" class="dot">'+i+'</a></li>'
            }
        }
        $('#dot-content').append(items);
        var imageContent = $('#image-content');
        imageContent.css({'width': parseInt(imageContent.find('li').length)+'00%'});
    },
    moveTo: function(element, direction, tempo){
        var slider = parseInt(element.data('slider'));
        var limitSliderNum, sliderCurrent, manageCurrent;
        if(tempo==undefined){
            tempo = 360;
        }
        if(direction=='<-'){
            limitSliderNum = 1;
            sliderCurrent = slider-2;
            manageCurrent = 'prev';
        }else if(direction=='->'){
            limitSliderNum = $('.image-content-item').length;
            sliderCurrent = slider;
            manageCurrent = 'next';
        }else if(direction=='.'){
            limitSliderNum = 0;
            sliderCurrent = slider-1;
            manageCurrent = 'dot';
        }
        if(slider!=limitSliderNum){
            $('#image-content').animate({
                'left': '-'+((sliderCurrent)*100)+'%'
            }, tempo, function(){
                $('.current').removeClass('current');
                $('.dotActive').removeClass('dotActive');
                if(manageCurrent=='prev'){
                    element.prev().addClass('current');
                    $('#dot-content').find('li[data-slider="'+(slider-1)+'"]').find('a.dot').addClass('dotActive');
                }else if(manageCurrent=='next'){
                    element.next().addClass('current');
                    $('#dot-content').find('li[data-slider="'+(slider+1)+'"]').find('a.dot').addClass('dotActive');
                }else if('dot'){
                    $('#image-content').find('li[data-slider="'+slider+'"]').addClass('current');
                    $('#dot-content').find('li[data-slider="'+(slider)+'"]').find('a.dot').addClass('dotActive');
                }
            });
        }
    },
    resetMove: function(element){
        $('#image-content').animate({'left': '0%'}, 360);
        element.removeClass('current');
        $('.image-content-item').first().addClass('current');
        $('.dotActive').removeClass('dotActive');
        $('.dot').first().addClass('dotActive');
    },
    setAutoMove: function(movement, tempo){
        if(tempo==undefined){
            tempo = 1000;
        }
        if(!movement){
            $('.pause').hide();
            $('.play').show();
            window.clearInterval(window.auto);
        }else{
            $('.pause').show();
            $('.play').hide();
            window.auto = window.setInterval(function(){
                var element = $('.current');
                var itemLength = $('.image-content-item').length
                var slider = parseInt(element.data('slider'));
                if( slider == itemLength ){
                    this.slider.resetMove(element);
                }else{
                    this.slider.moveTo(element, '->', 360);
                }
            }, tempo);
        }
    }
};
$(document).on('ready', function(){
    slider.init();
});
// slidear end //
$(document).ready(function(){
   var $win = window;
   var $pos = 150;

   $(window).scroll(function(){
      if($(this).scrollTop() > $pos){
         $("a.to-top").css("opacity","1");
         $("header").css("box-shadow","2px 5px 20px rgb(14,62,146,0.5)");
      }else{
         $("a.to-top").css("opacity","0");
         $("header").css("box-shadow","1px 2px 1px transparent");
      }
   })
});

$(".to-top").on("click",function(){
   $("html, body").animate({scrollTop:0},"slow");
});
// scroll up //